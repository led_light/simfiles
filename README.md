# The Complete led_light Collection

A Git repository for managing all of my custom simfile content for [StepMania] and [Project OutFox],
providing revision history along with a simple way to download my packs and receive updates
(which includes additional songs and bug fixes).

By default, all of my simfiles use DDR X-scale ratings and null (zero) offset,
but I have also made [Python scripts] that can modify the simfiles to use old DDR/ITG ratings or +9ms offset if you prefer.

For more information, head to my simfile home page on [Zenius -I- vanisher.com].

## 1-Click Download

If you just want to download my packs individually without using Git,
simply click on any of the download links below.
If you are using a theme that does not have `group.ini` support,
be sure to rename the group folder after the name of the pack as well.

- [led_light Complete Mix 0]
- [led_light Complete Mix 1]
- [led_light Complete Mix 2]
- [led_light Complete Mix 3]
- [K/DA - ALL OUT]
- [Friday Night Funkin']
- [Friday Night Funkin' B-Sides]
- [NIJISANJI EN Pad Pack]

## Download/Update All Packs with Git

### Initial Download

1. Install [Git] and [Git LFS] if you haven't already.
2. Clone the repository with `git clone --recurse-submodules --shallow-submodules https://gitlab.com/led_light/simfiles.git`.
   I recommend cloning into a new directory that is separate from StepMania's default `Songs` folder.
3. In your StepMania installation,
   open the `Preferences.ini` file,
   locate the `AdditionalSongFolders` parameter,
   and add the full path to the repository as the parameter value
   (e.g. `AdditionalSongFolders=/path/to/simfiles`).

And that's it!
The new songs should now be playable the next time you open StepMania.
If not, you can try one or more of the following solutions:

-   Run `Reload Songs/Courses` from the options menu.
-   Disable the `Fast Load` option.
-   Delete the `Cache` folder in your StepMania program directory.

### Downloading Updates

To receive future updates, simply run `git pull --recurse-submodules`!
Any additional content/updates will then be downloaded and automatically added to StepMania.

## License

Unless otherwise specified,
the `.sm` and `.ssc` chart files are subject to the terms under the [CC-BY-NC-ND-4.0] license,
but additional rights can be granted upon request with explicit written permission by me.

The copyright for all music, videos, and graphics still belong to their respective owners.

<!-- Links -->

[StepMania]: https://github.com/stepmania/stepmania
[Project OutFox]: https://projectoutfox.com/

[Python scripts]: https://github.com/edward-ly/simfile-mods
[Zenius -I- vanisher.com]: https://zenius-i-vanisher.com/v5.2/viewsimfilecategory.php?categoryid=821

[led_light Complete Mix 0]: https://gitlab.com/led_light/simfiles-complete-mix-0/-/archive/master/simfiles-complete-mix-0-master.zip
[led_light Complete Mix 1]: https://gitlab.com/led_light/simfiles-complete-mix-1/-/archive/master/simfiles-complete-mix-1-master.zip
[led_light Complete Mix 2]: https://gitlab.com/led_light/simfiles-complete-mix-2/-/archive/master/simfiles-complete-mix-2-master.zip
[led_light Complete Mix 3]: https://gitlab.com/led_light/simfiles-complete-mix-3/-/archive/master/simfiles-complete-mix-3-master.zip
[K/DA - ALL OUT]: https://gitlab.com/led_light/simfiles-kda/-/archive/master/simfiles-kda-master.zip
[Friday Night Funkin']: https://gitlab.com/led_light/simfiles-fnf/-/archive/master/simfiles-fnf-master.zip
[Friday Night Funkin' B-Sides]: https://gitlab.com/led_light/simfiles-fnfb/-/archive/master/simfiles-fnfb-master.zip
[NIJISANJI EN Pad Pack]: https://gitlab.com/led_light/simfiles-nijien/-/archive/master/simfiles-nijien-master.zip

[Git]: https://git-scm.com/
[Git LFS]: https://git-lfs.github.com/

[CC-BY-NC-ND-4.0]: https://creativecommons.org/licenses/by-nc-nd/4.0/
